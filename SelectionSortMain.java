import java.util.Arrays;
public class SelectionSortMain
{
public static int[] SelectionSort(int arr[]){
for(int i=0; i<arr.length-1;i++){
int index=i;
for(int j=i+1; j<arr.length; j++)
if(arr[j]<arr[index])
index=j;
int smallerNumber=arr[index];
arr[index]=arr[i];
arr[i]=smallerNumber;

}
return arr;
}

public static void main(String a[])
	{
	int arr[]={40,10,-30,45,39,32};
	System.out.println("Before sorting: ");
	System.out.println(Arrays.toString(arr));
	arr=SelectionSort(arr);
	System.out.println("===========");
	System.out.println("After sorting: ");
	System.out.println(Arrays.toString(arr));
	}
	}
	